Meteor.methods({

  clearPeers () {
    Peers.remove({});
    Peers.insert({man: false, girl: false, trans: false})
  },

  changePeers (peers) {
    Peers.update(peers._id, peers);
  }

});
