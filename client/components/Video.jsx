Video = React.createClass({
  mixins: [ReactMeteorData],

  clearPeers () {
    Meteor.call('clearPeers');
  },

  getInitialState () {
    return {
      person: false
    }
  },

  getMeteorData () {
    var peers = Peers.findOne();
    var peersReady = false;

    if (peers && peers.man && peers.girl && peers.trans) {
      peersReady = true;
    }

    return {peers, peersReady};
  },

  changePeers (peers, param) {
    if (this.state.updated != true) {
      this.setState({updated: true});
      Meteor.call('changePeers', peers);
    }

    if (this.state.transAfter != true && param == 'trans') {
      this.setState({transAfter: true});
      Meteor.call('changePeers', peers);
    }

    if (this.state.girlAfter != true && param == 'girl') {
      this.setState({girlAfter: true});
      Meteor.call('changePeers', peers);
    }
  },

  runChat () {
    console.log('runChat');
    var person = this.state.person;
    if (person == 'man') {
      console.log('man no signaling ===================================================');
    } else if (person == 'girl') {

      console.log('girl signal man =====================================================');
      var girl_man = this.state.girl_man;
      var man_girl_signal = this.data.peers.man.man_girl_signal;
      girl_man.signal(man_girl_signal);
      console.log(this.data.peers);

    } else if (person == 'trans') {

      console.log('trans signal man & girl =============================================');
      var trans_man = this.state.trans_man;
      var trans_girl = this.state.trans_girl;
      var man_trans_signal = this.data.peers.man.man_trans_signal;
      var girl_trans_signal = this.data.peers.girl.girl_trans_signal;
      trans_man.signal(man_trans_signal);
      trans_girl.signal(girl_trans_signal);
      console.log(this.data.peers);
    }
  },

  componentDidUpdate () {

    var { person,
          man_girl,
          man_trans,
          man_girl_signal,
          man_trans_signal,
          girl_man,
          girl_trans,
          girl_trans_signal,
          /* girl_man_signal , */
          trans_man,
          trans_girl,
          /* trans_man_signal , */
          /* trans_girl_signal */
        } = this.state;

    var peers = this.data.peers;
    var person = this.state.person;

    var girl_man_signal,
        trans_man_signal,
        trans_girl_signal;

    if ( peers && peers.girl) {
      girl_man_signal = peers.girl.girl_man_signal;
    }

    if ( peers && peers.trans) {
      trans_man_signal = peers.trans.trans_man_signal;
      trans_girl_signal = peers.trans.trans_girl_signal;
    }

    if (this.data.peersReady && !this.state.chatRuned) {
      this.setState({chatRuned: true});
      this.runChat();
    }

    if ( this.state.chatRuned &&
         !this.state.manRuned &&
         girl_man_signal &&
         trans_man_signal &&
         person == 'man' ) {
      this.setState({manRuned: true});
      console.log('================= man signal girl & trans ====================');
      man_girl.signal(girl_man_signal);
      man_trans.signal(trans_man_signal);
      console.log(this.data.peers);
    }

    if ( this.state.chatRuned &&
         !this.state.girlRuned &&
         trans_girl_signal &&
         person == 'girl'
       ) {
      this.setState({girlRuned: true});
      console.log('================= girl signal trans ==========================');
      girl_trans.signal(trans_girl_signal);
      console.log(this.data.peers);
    }


    if ( !this.state.chatRuned ) {

      if (person == 'man' && man_girl_signal && man_trans_signal) {
        peers.man = {man_girl_signal, man_trans_signal};
        this.changePeers(peers);
      } else if (person == 'girl' && girl_trans_signal) {
        peers.girl = { girl_trans_signal };
        this.changePeers(peers);
      } else if (person == 'trans' && trans_man && trans_girl) {
        peers.trans = "ready";
        this.changePeers(peers);
      }
    } else {
      if (person == 'trans' &&
          this.state.trans_man_signal &&
          this.state.trans_girl_signal) {
        peers.trans = { trans_man_signal: this.state.trans_man_signal,
                        trans_girl_signal: this.state.trans_girl_signal};
        this.changePeers(peers, 'trans');
      }
      if (person == 'girl' &&
          this.state.girl_trans_signal &&
          this.state.girl_man_signal) {
        peers.girl = { girl_trans_signal: this.state.girl_trans_signal,
                       girl_man_signal: this.state.girl_man_signal };
        this.changePeers(peers, 'girl');
      }
    }

  },

  joinPerson (person) {
    var stream = this.state.stream;
    var man_girl, man_trans,
        girl_man, girl_trans,
        trans_man, trans_girl;

    if (person == 'man') {
      man_girl = new SimplePeer({initiator: true, stream: stream});
      man_trans = new SimplePeer({initiator: true, stream: stream});

      this.setState({ man_girl, man_trans, person });

      man_girl.on('signal', (data) => {
        this.setState({ man_girl_signal: data});
      });
      man_trans.on('signal', (data) => {
        this.setState({ man_trans_signal: data});
      });

      // man handle streams
      man_girl.on('stream', (stream) => {
        var video = document.querySelector('#girl-video');
        video.src = window.URL.createObjectURL(stream);
        video.play();

        var self_video = document.querySelector('#man-video');
        self_video.src = window.URL.createObjectURL(this.state.stream);
        self_video.play();
      });

    } else if (person == 'girl') {
      girl_trans = new SimplePeer({initiator: true, stream: stream});
      girl_man = new SimplePeer({stream: stream});

      this.setState({ girl_man, girl_trans, person });

      girl_trans.on('signal', (data) => {
        this.setState({ girl_trans_signal: data});
      });
      girl_man.on('signal', (data) => {
        this.setState({ girl_man_signal: data});
      });

      // girl handle streams
      girl_man.on('stream', (stream) => {
        var video = document.querySelector('#man-video');
        video.src = window.URL.createObjectURL(stream);
        video.play();

        var self_video = document.querySelector('#girl-video');
        self_video.src = window.URL.createObjectURL(this.state.stream);
        self_video.play();
      });

    } else {
      trans_man = new SimplePeer();
      trans_girl = new SimplePeer();

      trans_man.on('signal', (data) => {
        this.setState({trans_man_signal: data});
      });
      trans_girl.on('signal', (data) => {
        this.setState({trans_girl_signal: data});
      });

      // trans handle streams
      trans_man.on('stream', (stream) => {
        var video = document.querySelector('#man-video');
        video.src = window.URL.createObjectURL(stream);
        video.play();
      });
      trans_girl.on('stream', (stream) => {
        var video = document.querySelector('#girl-video');
        video.src = window.URL.createObjectURL(stream);
        video.play();
      });

      this.setState({ trans_man, trans_girl, person });
    }

  },

  getStream (stream) {
    this.setState({
      stream: stream
    });
  },

  componentDidMount () {
      navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
      navigator.getUserMedia({ video: true,
                               audio: true },
                               this.getStream,
                               err => console.log(err));
  },

  render () {
    return (
      <div>
        <div>
          <button id="man-button" onClick={this.joinPerson.bind(this, 'man')}>man</button>
          <button id="girl-button" onClick={this.joinPerson.bind(this, 'girl')}>girl</button>
          <button id="trans-button" onClick={this.joinPerson.bind(this, 'trans')}>translator</button>
          <button id="clear-peers" onClick={this.clearPeers}>clear peers</button>
        </div>
        <div>
          <video id="man-video"></video>
          <video id="girl-video"></video>
        </div>
      </div>
    );
  }
});

Meteor.startup(() => {
  React.render((<Video />), document.getElementById('render-video'));
});
